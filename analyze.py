import pandas as pd
from run_cdc import PathDirectory
from tabulate import tabulate
import os
class analyze_data(PathDirectory):
    def __init__(self):
        super().__init__()
        try:
            self.working_csv = pd.read_csv(os.path.join(self.LogDir,'temp.csv'))
        except FileNotFoundError:
            print("File csv not found, proceed to create new temp.csv")
        except pd.errors.EmptyDataError:
            print("CSV File was found, but its empty, proceed to create new temp.csv")
        except Exception:
            print("Some other exception")

    def show_table(self,filter_source='None'):
        #filter_source => 'accounts','cards' or 'savings_accounts'
        if filter_source != None :
            data = self.working_csv[self.working_csv['source']==filter_source]
        else:
            data = self.working_csv
        data = data.sort_values('time_stamp')
        print(tabulate(data,headers='keys',tablefmt='simple',showindex='never'))

    def re_open_CSV(self):
        try:
            self.working_csv = pd.read_csv(os.path.join(self.LogDir,'temp.csv'))
            print('reopen CSV success!')
        except FileNotFoundError:
            print("File csv not found, proceed to create new temp.csv")
        except pd.errors.EmptyDataError:
            print("CSV File was found, but its empty, proceed to create new temp.csv")
        except Exception:
            print("Some other exception")

if __name__ == '__main__':
    working_data = analyze_data()
    while(True):
        try:
            keyboard_input = input('Send command here!\nfor example: showtable accounts\n')
            list_keyboard_input = keyboard_input.split(' ')
            if list_keyboard_input[0] == 'showtable':
                try :
                    filter       = list_keyboard_input[1]
                except IndexError:
                    filter       = None
                working_data.show_table(filter_source=filter)
            elif list_keyboard_input[0] == 'reopen_csv':
                working_data.re_open_CSV()
            elif keyboard_input == 'exit':
                break
            else:
                print('Command not found')
        except KeyboardInterrupt:
            print('KeyboardInterrupt is found, type exit to stop the program')
            continue
